# *********************
# load C++ libraries
import ROOT
import os
package = 'DJSExtendedAnalysis'
headers = ['SimpleEResponse.h','PUCorrection.h'] 

isAnalysiBase = 'AtlasArea' in os.environ or 'AnalysisBase_VERSION' in os.environ
libName = "lib"+package + ('Lib.so' if isAnalysiBase else '.so')
ROOT.gSystem.Load(libName)

libPath= [ p for p in ROOT.gSystem.GetLibraries().split(' ') if libName in p][0]
incPath = os.path.join( os.path.dirname(os.path.dirname(libPath)), 'include', package,)
# make ROOT C++ interpreter knows about the C++ class defined in the above libraries.
# it is enough to load the C++ headers.
for h in headers:
    ROOT.gInterpreter.LoadFile( os.path.join(incPath, h ) )
del(package)
del(headers)
# *********************
