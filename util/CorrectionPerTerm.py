#!/usr/bin/env python
from optparse import OptionParser
from ROOT import *
import testPUCorrection as tpu

#Can use graphVsX and write something like multigraphVsX

def  GraphPerTerm(X, xvals,  other, v, opt="L",  puv=None, yrange=(0.7,1.10), term=0 ,**args):
    #global canvas
    #canvas = canvas or ROOT.TCanvas("c","C", 1200, 1000)
    puv = puv or tpu.PUvars()
    args['opt']=None
    #termfunc = tpu.puCorrHelper.correctionFactorPerTerm
    termfunc = tpu.puCorrHelper.correctedPtPerTerm
    graph0 = tpu.graphVsX(X,xvals, puv=puv.update(other,v),func=termfunc, nterm=term, **args) #v is the value for variable other. Need to fix the func call

    return graph0

def Graph3DTerms(outfile,abseta,canvas):

    canvas.Clear()
    g0 = GraphPerTerm('pt', [10+i*5 for i in range(-20,80)] , other='abseta', v=abseta, term=0) 
    g0.SetMaximum(20.0)
    g0.SetMinimum(-100.0)
    g0.Draw()
    g1 = GraphPerTerm('pt', [10+i*5 for i in range(-20,80)] , other='abseta', v=abseta, term=1)
    g1.SetLineColor(kBlue)
    g1.Draw('SAME')
    g2 = GraphPerTerm('pt', [10+i*5 for i in range(-20,80)] , other='abseta', v=abseta, term=2)
    g2.SetLineColor(kRed)
    g2.Draw('SAME')
    gall = GraphPerTerm('pt', [10+i*5 for i in range(-20,80)] , other='abseta', v=abseta, term=999)
    gall.SetLineColor(kGreen)
    gall.Draw('SAME')
    #gCorrected3D = GraphPerTerm('pt', [10+i*5 for i in range(-20,80)] , other='abseta', v=abseta, term=1000)
    #gCorrected3D.SetLineColor(kViolet-1)
    #gCorrected3D.Print()
    #gCorrected3D.Draw('SAME')
    l = TLegend(0.2,0.3,0.4,0.5)
    l.AddEntry(g0,'1st term','l')
    l.AddEntry(g1,'2nd term','l')
    l.AddEntry(g2,'3rd term','l')
    l.AddEntry(gall,'Sum','l')
    #l.AddEntry(gCorrected3D,'1-Sum','l')
    etastr = '|#eta|='+str(abseta)
    leta = TLatex()
    leta.DrawLatexNDC(0.2,0.2, etastr)
    l.Draw('SAME')
    canvas.Print(outfile,"eta"+str(abseta))

def GraphDeltaPtTerms(outfile,abseta,canvas):
    #Same as above with term 4 and 5
    #Implement term 4 and 5 in .h file
    canvas.Clear()
    g3 = GraphPerTerm('pt', [10+i*5 for i in range(-20,80)] , other='abseta', v=abseta, term=3) 
    g3.SetMaximum(20.0)
    g3.SetMinimum(-100.0)
    g3.Draw()
    g4 = GraphPerTerm('pt', [10+i*5 for i in range(-20,80)] , other='abseta', v=abseta, term=4) 
    g4.SetLineColor(kBlue)
    g4.Draw('SAME')
    g5 = GraphPerTerm('pt', [10+i*5 for i in range(-20,80)] , other='abseta', v=abseta, term=5) 
    g5.SetLineColor(kRed)
    g5.Draw('SAME')
    l = TLegend(0.2,0.3,0.4,0.5)
    l.AddEntry(g3,'1st term','l')
    l.AddEntry(g4,'2nd term','l')
    l.AddEntry(g5,'sum','l')
    l.Draw('SAME')
    etastr = '|#eta|='+str(abseta)
    leta = TLatex()
    leta.DrawLatexNDC(0.2,0.2, etastr)
    l.Draw('SAME')
    canvas.Print(outfile,"eta"+str(abseta))

if __name__ == '__main__':
    outfile = 'testPlots.pdf'
    canvas = TCanvas("c","C", 1200, 1000)
    canvas.Print(outfile+"[")
    #abseta = 0.1
    #for i in range(45):
    #    abseta = 0.1*i
    #    Graph3DTerms(outfile,abseta,canvas)
    #canvas.Print(outfile+"]")

    #termfunc = tpu.puCorrHelper.correctionFactorPerTerm
    #puv = tpu.PUvars()
    #g3D = tpu.graphVsX('pt', [10+i*5 for i in range(80)], puv=puv.update('abseta',0.1))
    #g3D.SetLineColor(kGreen)
    #g3D.Draw('SAME')
    #canvas.Print(outfile,"Sum")

    #Now the Delta pT term
    outFileDeltaPt = 'testDeltaPtPlots.pdf'
    canvas2 = TCanvas("c","C", 1200, 1000)
    canvas2.Print(outFileDeltaPt+"[")
    for i in range(45):
        abseta = 0.1*i
        GraphDeltaPtTerms(outFileDeltaPt,abseta,canvas2)
    canvas2.Print(outFileDeltaPt+"]")

