#!/usr/bin/env python
from optparse import OptionParser
from ROOT import *
import testPUCorrection as tpu

pu = tpu.PUvars()
canvas = TCanvas("c","C", 1200, 1000)
outfile = 'testExtrapolation_AllMC.pdf'
canvas.Print(outfile+"[")

for i in range(45):
    eta = 0.1*i
    pu.update('abseta',eta)
    h = tpu.muNPVMap(puv=pu)
    canvas.Print(outfile)

canvas.Print(outfile+"]") 
