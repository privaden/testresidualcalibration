import ROOT
#import DJSExtendedAnalysis.LoadCppLibs
ROOT.gInterpreter.LoadText( open("PUCorrection.h").read() )

# Create the correction helper 
puCorrHelper = ROOT.PUCorrection.PUCorrectionHelper()

# load the standalone root file containing the calib parameters
#puCorrHelper.loadParameters("residualCalibParameters_mc16d.root")
puCorrHelper.loadParameters("residualCalibParameters_AllMC.root")
#puCorrHelper.loadParameters("/home/lalloue/ufo/source/DeriveJetScales/scripts/residualCalibParameters_CSSKUFO.root")


if 0:
    # Test filling empty bins ath hig/low mu and NPV and then doing interpolation
    print 'filling empty in 0'
    import DJSExtendedAnalysis.LoadCppLibs
    for h in puCorrHelper.m_3Dp0_vs_muNPV:
        ROOT.DJS.fillEmptyBin(h, -999)
    print 'filling empty in 1'
    for h in puCorrHelper.m_3Dp1_vs_muNPV:
        ROOT.DJS.fillEmptyBin(h, -999)
    print 'filling empty in 2'
    for h in puCorrHelper.m_3Dp2_vs_muNPV:
        ROOT.DJS.fillEmptyBin(h, -999)


def testOnFile():
    # reco pT = 122.409  eta = 0.749862  --> 325.037
    f = ROOT.TFile("../Data/SmallR/DiJets/JZ2_evtweights.root")
    t = f.AntiKt4CHSUFOJets

    for i in range(10):
        t.GetEntry(i)
        #print i, t.pt_reco.size()
        #puCorrHelper.correctionFactor(40 ,10.1 ,0.5 , 10000, 20,20)
        for j in range(t.pt_reco.size()):
            cpt = puCorrHelper.correctedPt(t.pt_reco[j] , abs(t.eta_reco[j]) , t.area[j] , t.rho, int(t.mu),int(t.NPV))
            print "reco pT", t.pt_reco[j] , "eta =",  abs(t.eta_reco[j]) , "--> ", cpt


def relCorr3D(pt = 30, abseta = 0.1, area=0.5, rho=8500, mu=30, NPV=18):

    areaCorr = area*rho*0.001;

    pt_ref = 170.0 if pt > 170.0  else pt 
    c3D = puCorrHelper.correction3D(pt_ref - areaCorr ,abseta,mu,NPV)
    return c3D/pt

def relCorr3D_noextrap(pt = 30, abseta = 0.1, area=0.5, rho=8500, mu=30, NPV=18):

    areaCorr = area*rho*0.001;

    pt_ref = 170.0 if pt > 170.0  else pt 
    c3D = puCorrHelper.correction3D_noextrap(pt_ref - areaCorr ,abseta,mu,NPV)
    return c3D/pt

def diff_corr3D(*args):
    return relCorr3D(*args) - relCorr3D_noextrap(*args)


class PUvars:
    """Just to conveniently describe the set of 5 variables used for PU corrction """
    vformat = dict(
        pt='pt={:.1f}',
        abseta='abseta={:.2f}',
        area='area={:.2f}',
        rho='rho={:.2f}',
        mu='mu={}',
        NPV='NPV={}',)
    
    def __init__(self,pt = 30, abseta = 0.11, area=0.5, rho=8500, mu=30, NPV=18):
        self.pt = pt
        self.abseta =abseta        
        self.area= area
        self.rho= rho
        self.mu= mu
        self.NPV=NPV

    def update(self, k, v):
        setattr(self,k,v)
        return self
    
    def values(self, **args ):
        for k,v in args.iteritems():
            setattr(self, k,v )
        return (self.pt, self.abseta, self.area, self.rho, self.mu, self.NPV)

    def valueDesc(self, vname):
        return self.vformat[vname].format(getattr(self,vname))
    def allValueAsStr(self):
        l = [ self.valueDesc(v)  for v in ['pt', 'abseta', 'area','rho','mu','NPV'] ]
        return l

    
    
def graphVsX(X, xvals, puv=None, opt="AL", func=None, xtext=0.5, ytext=0.5,nterm=-1):
    """Creates the TGraph of PU correction as a function of variable X and for the values of X given by xvals
    """
    # by default calculate the PU correction with puCorrHelper.correctionFactor
    func = func or puCorrHelper.correctionFactor
    #  (to plot the relative 3d correction, call with func=relCorr3D )

    puv = puv or PUvars()
    N=len(xvals)
    g = ROOT.TGraph(N)
    for i,v in enumerate(xvals):
        if nterm>=0:
            print puv.update(X,v).values()
            arguments = puv.update(X,v).values()
            arglist = list(arguments)
            arglist = arglist + [nterm]
            arguments = tuple(arglist)
            print arguments
            g.SetPoint(i, v, func( *arguments) )
        else:
            g.SetPoint(i, v, func( *puv.update(X,v).values()) )
            
    #g.SetTitle(";{};correction factor".format(X))
    g.SetTitle(";{};#Delta pT".format(X))
    g.SetLineWidth(2)

    if opt != None:
        g.Draw(opt)
        l = ROOT.TLatex()
        l.SetNDC()
        for t in puv.allValueAsStr():
            l.DrawLatex(xtext,ytext, t)
            ytext-=0.07
    return g

canvas = None
def multigraphVsX(X, xvals,  other, ovals, opt="L",  puv=None, yrange=(0.7,1.10) ,**args):
    """Same as graphVsX but draw multiple TGraph vs X, each with 1 of the other variable 'other' varied according to ovals """
    global canvas
    canvas = canvas or ROOT.TCanvas("c","C", 1200, 1000)
    puv = puv or PUvars()
    args['opt']=None

    # 
    graphL = [ graphVsX(X,xvals, puv=puv.update(other,v), **args ) for v in ovals]
    pref='A'

    leg = ROOT.TLegend( 0.5, 0.11, 0.8, 0.3)
    leg.SetNColumns(2)
    colors=[1, 2, 3, 4, 5, 6, 7, 8, 9]
    for i in range(len(graphL)):
        g = graphL[i]
        g.SetLineColor(colors[i])
        g.Draw(pref+opt)
        leg.AddEntry(g, puv.vformat[other].format(ovals[i]) )
        pref=''
    graphL[0].GetYaxis().SetRangeUser(yrange[0], yrange[1])
    for v in ['pt', 'abseta', 'area','rho','mu','NPV']:
        if v==X or v==other:continue
        leg.AddEntry(None,puv.valueDesc( v ) ,"")
    leg.Draw()        
    return leg, graphL



def muNPVMap( puv=None , func=None, title="correction factors"):
    puv = puv or PUvars()
    func = func or puCorrHelper.correctionFactor

    h2 = ROOT.TH2F("muNPVMap", title+";mu;NPV",70,0,70,50,0,50)

    for npv in range(50):
        for mu in range(70):
            #print " fill ",npv,mu, func( *puv.values(mu=mu, NPV=npv) )
            h2.Fill(mu,npv, func( *puv.values(mu=mu, NPV=npv) ) )
    h2.SetStats(0)
    h2.Draw("colz")
    return h2
                                        
                
def dumpManyPlots(outfile='pucorrections_AllMC.pdf'):
    global canvas
    canvas = canvas or ROOT.TCanvas("c","C", 1200, 1000)
    #canvas = ROOT.TCanvas()
    canvas.Print(outfile+"[")

    
    g = multigraphVsX('pt', [10+i*5 for i in range(80)] , other='NPV', ovals=[15, 20,30,40,])
    canvas.Print(outfile,"vsPt_varyNPV")
    g = multigraphVsX('pt', [10+i*5 for i in range(80)] , other='mu', ovals=[15, 20,30,40,])
    canvas.Print(outfile,"vsPt_varymu")
    g = multigraphVsX('pt', [10+i*5 for i in range(80)] , other='abseta',yrange=(0.7,1.5), ovals=[0., 0.3, 0.5, 1.0, 1.5,3.0, 3.3,3.5])
    canvas.Print(outfile,"vsPt_varyNPV")

    g = multigraphVsX('abseta', [i*0.02 for i in range(200)] , other='pt',yrange=(0.7,2.2), ovals=[20,30, 50, 100, 200], )
    canvas.Print(outfile,"vsEta")
    g = multigraphVsX('mu', range(60) , other='pt', ovals=[30, 50, 100, 200], )
    canvas.Print(outfile,"vsMu")
    g = multigraphVsX('NPV', range(50) , other='pt', ovals=[30, 50, 100, 200], )
    canvas.Print(outfile,"vsNPV")

    h2 = muNPVMap()
    canvas.Print(outfile,"map vs mu NPV")

    h2d = muNPVMap(func=diff_corr3D, title="correction_3D/pt : delta with/without extrapolation")
    canvas.Print(outfile,"delta vs mu NPV")

    
    canvas.Print(outfile+"]")

    
#dumpManyPlots()
