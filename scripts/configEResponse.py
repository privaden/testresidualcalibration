###############################################
##
## Example configuration file with custom extension of the framework.
## 
## This file illustrates how to modify the configExample.py config file in order
## to :
##  - read additionnal input from the TTree into the JetData class
##  - Create a new JetProcessor which makes use of this input and fills histograms
##  - configure everything
##
## Added config w.r.t configExample.py are marked with the EXTENSION comment

from DeriveJetScales.DefaultConfigs import solveConfigDependencies, configToCalibrationObjects, confBinnedTH1
from DeriveJetScales.CalibrationDerivation import CalibrationDerivation
from BinnedPhaseSpace.BinnedPhaseSpaceConfig import AxisSpec , BinnedPhaseSpace, BinnedTH1
import DJSExtendedAnalysis.LoadCppLibs
from DeriveJetScales.PyHelpers import seq
from DeriveJetScales.DefaultConfigs import configToFitOperations

## ******************************************************
## EXTENSION
## we include a utility (copy it from DeriveJetScales/utils/)
execfile('generateNewJetDataClass.py')
# we can now invoke generateNewJetDataClass to build on-the-fly a new JetData class
generateJetDataClass("JetData_ext", # the name of the new class. It inherits JetData
                     [], # list of new jet variables (which are vector variable in the input). Give a tuple ('type', 'varname')
                     scalarVars=[('int','NPV')] # list of scalar variables in the input .
)


# Next we define a new JetProcessor.
# Here it's simple enough we directly pass the c++ definition in a string ...

## **************************************



def configure():
    # this function is required by the framework. It is expected it returns a CalibrationObjects fully configured.
    # For this we're going to :
    #  - import a pre-existing configuration
    #  - adapt it to our need (ex: branch name in our input TTree)
    #  - call the helper functions to translate the config into a CalibrationObjects
    #  
    
    # ------------------------------------------------------
    # retrieve a default configuration :
    from DeriveJetScales.DefaultAntiKt4 import  config
    # we just imported the AntiKt10LCTopoTrimmed configuration under the variable 'config'    
    # see DefaultAntiKt10LCTopoTrimmed.py to understand what it contains by default.

    # ------------------------------------------------------
    # we can now update it as we like
    # for example : 
    config.jmsRespEvaluation = 'gaus+mode'
    config.jmsNIopts.directNumInv = True

    #config.CalibrationDerivationClass = CalibrationDerivation

    jetType= ConfigDict.cmdLineArgs.jetType

    ## **************************************
    ## EXTENSION 
    # declare our new class and input variables 
    config.JetDataClass = "DJS::JetData_ext"  # change the default JetData clas
    config.inputBranches.update(   # declare additionnal branches
        NPV = "NPV")
        #D2 = jetType+"JetsAuxDyn.D2", )
    # request a new JetDataFiller for our new JetData class :
    config.extendedJetDataFiller.update(npvFiller = config.JetDataClass )#????
    # (--> the system will create a JetDataFiller for the new class and set it under 'objs.npvFiller')
    ## **************************************

    
    # ----------------------------------
    # below we set the branch names corresponding to our input TTree
    #  in this example, we use a xAOD as input and pick the jet type from the script's arguments (this is automatically
    #  set-up from the command line invocation invocation)
    config.inputBranches.update( 
        eventWeight = "eventWeightXS",
        e_true = "e_true",
        e_reco = "e_reco",
        eta_true = "eta_true",
        eta_reco = "eta_reco",
        m_true = "m_true",
        eta_det= "",
        mu= 'mu',
    )

    config.energyUnit = 1

    if config.massType == "mCalo":
        config.inputBranches.m_reco = jetType+"JetsAux.m"
    elif config.massType == "mTA":			 
        config.inputBranches.m_reco = jetType+"JetsAuxDyn.m_ta"
    # elif config.massType == "mComb": NOT ready yet			 
    #     config.inputBranches.m_reco = jetType+"JetsAux.m"

    config.my_eBins = AxisSpec("eBins","eBins", [20, 25, 30, 35, 40, 50, 60, 80, 100, 120, 150, 200, 240, 300, 400, 500, 600,
                                                 800, 1000, 1200, 1500, 2000, 2500, 3000, 3500, 4000, 5500 ],)
    config.my_etaBins =AxisSpec("etaBins","eta", seq(-4.5 , 4.5, 0.2 ) , isGeV=False)
    
    config.userObjects.my_jesBPS = BinnedPhaseSpace("my_jesBPS", config.my_eBins, config.my_etaBins)

    ## **************************************
    ## EXTENSION 
    # describe a custon steps using our new JetProcessor :
    config.steps.jesAllResp = [
        ConfigDict(klass=ROOT.DJS.EResponseCollector,
                   outFileRef = "fOut_jes",
                   settings=ConfigDict( requiredData = ['jes', 'npvFiller'] ) , # make sure we declare our extended JetDataFiller (npvFiller)
                   pointers = ConfigDict(
                       eR_in_EEta = confBinnedTH1('my_jesBPS', "jes_rBins", title="E response;E response;", save=True, ),
                       deltaEta_in_EEta = confBinnedTH1('my_jesBPS', "jes_deltaEtaBins", title="#Delta eta ;#Delta #eta;", save=True),
                       m_bps = "my_jesBPS",
                   ),
        ) ] + config.steps.EtaJES_closureresp + [
            ConfigDict(klass=ROOT.DJS.EResponseCollector,
                       outFileRef = "fOut_jes",
                       settings=ConfigDict( requiredData = ['jes', 'npvFiller'] ) , # make sure we declare our extended JetDataFiller (npvFiller)
                       pointers = ConfigDict(
                           eR_in_EEta = confBinnedTH1('my_jesBPS', "jes_rBins", title="E response;E response;", save=True, name="eR_in_EEta_JES" ),
                           deltaEta_in_EEta = confBinnedTH1('my_jesBPS', "jes_deltaEtaBins", title="#Delta eta ;#Delta #eta;", save=True, name="deltaEta_in_EEta_JES"),
                           m_bps = "my_jesBPS",
                    ),        
            )
        ]
    
    config.steps.EResponseCollector = [ #
        ConfigDict(klass=ROOT.DJS.EResponseCollector,
                   outFileRef = "fOut_jes",
                   settings=ConfigDict( requiredData = ['jes', 'npvFiller'] ) , # make sure we declare our extended JetDataFiller (npvFiller)
                   pointers = ConfigDict(
                       eR_in_EEta = confBinnedTH1('my_jesBPS', "jes_rBins", title="E response;E response;", save=True, ),
                       deltaEta_in_EEta = confBinnedTH1('my_jesBPS', "jes_deltaEtaBins", title="#Delta eta ;#Delta #eta;", save=True),
                       m_bps = "my_jesBPS",
                   ),
        )
    ]
    # this step can be exectued by passing the command line option '-s User.simpleDist' 
    #****************************************

    
    # ------------------------------------------------------
    # The default 'config' contains unset options because we want these options to be set 
    # to values depending on other options (unset options are those having the value USE_DEFAULT )
    # 
    # To solve these dependencies and calculate values for these unset options, we call the function below.
    # Note that if we had manually set such unset options in the line above (so their value is no more USE_DEFAULT), then they won't be overwritten by the call.
    solveConfigDependencies(config)

    # we have a last chance to modify the unset options which have just been calculated
    # in solveConfigDependencies().
    # This is useful if we want to re-use their calculated values. For ex:
    # config.jmsWorkDir = config.jmsWorkDir+'_mytestDir/'


    # ------------------------------------------------------
    # Finally convert all the options inside config into real
    # C++ or python objects to be used during derivation :
    objs = configToCalibrationObjects(config)

    objs.jesFitOperations = configToFitOperations( objs, ConfigDict( evaluationMode="gaus+mode",
                                                                     saveFuncs=False,
                                                                     binSystem="EEta",
                                                                     balanceFitterName="balanceFitter_jes") )

    # objs is now a fully configured CalibrationObjects and ready to
    # be returned to the framework.
    return objs
    


