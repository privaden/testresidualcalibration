// this file is -*- C++ -*-
#ifndef DJSANALYSIS_SIMPLEERESPONSE_H
#define DJSANALYSIS_SIMPLEERESPONSE_H


#include "DeriveJetScales/ResponseCollector.h"


namespace DJS {



  class EResponseCollector : public JetProcessor, public MutexBlock {
  public:
    virtual void init_cpp(CalibrationObjects &){   m_bps = eR_in_EEta->m_phaseSpace; setNMutex(m_bps->nBins());}
    
    virtual bool process(JetData &j, CalibrationObjects &  ){
      
      float e = m_binInETrue ? j.e_true : j.e_reco;
      int bin = m_bps->findBin(e, j.eta_det);
      //std::cout<< "  e="<<e <<"  eta="<< j.eta_det << "   bin="<<bin << std::endl; 
      if( bin == -1 ) return true;
      j.jes_valid = true;
      
      double eResp = j.e_reco/j.e_true;

      //std::cout<< j.e_true<<"  deta="<<  j.eta_reco - j.eta_true <<"  eta_true="<< j.eta_true << " eta_reco=" <<j.eta_reco << "   bin="<<bin << std::endl; 
      lock(bin); // protection in case we run multithreaded
      eR_in_EEta->at(bin)->Fill(eResp, j.weight) ;
      deltaEta_in_EEta->at(bin)->Fill(j.eta_reco-j.eta_true, j.weight) ;
      unlock(bin);

      return true;
    }
    

    BPS::BinnedPhaseSpace * m_bps =nullptr;
    bool m_binInETrue;

    BPS::BinnedTH1 * eR_in_EEta = nullptr ;
    BPS::BinnedTH1 * deltaEta_in_EEta = nullptr;

    
  };
  


}

#endif

